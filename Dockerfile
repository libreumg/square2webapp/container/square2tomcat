ARG TOMCAT_VERSION=9.0.52-jre11-openjdk-slim-bullseye

FROM tomcat:$TOMCAT_VERSION

ARG SQ2_VERSION=

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-java&name=Square2&sort=version&direction=desc&version=$SQ2_VERSION" /tmp/war_meta

RUN apt-get -y update ; apt-get -y install curl gawk jq

RUN export curr_sq2=$(jq -r '.items[0].version' /tmp/war_meta) && \
    curl "https://packages.qihs.uni-greifswald.de/repository/ship-snapshot-java/war/Square2/$curr_sq2/Square2-$curr_sq2.war" --output /usr/local/tomcat/webapps/Square2.war

RUN mkdir /etc/tomcatsquare2/

COPY square2.properties /etc/tomcatsquare2/square2.properties

EXPOSE 8080
